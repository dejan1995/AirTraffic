import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { AirplaneService } from '../../../services/airplane.service';
import { GeoLocationService } from './../../../services/geo-location.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Air } from '../model/airplane-model';

@Component({
  selector: 'app-airplane-list',
  templateUrl: './airplane-list.component.html',
  styleUrls: ['./airplane-list.component.css']
})
export class AirplaneListComponent implements OnInit {


  public subscriptionLocation: Subscription;
  public subscriptionAirplaneList: Subscription;
  public location: Coordinates;

  public airplaneF: any;
  public airplaneList = [];
  public airList: Air;
  private locationSource: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  public location$: Observable<any> = this.locationSource.asObservable();

  constructor(private airplaneService: AirplaneService, private geoLocationService: GeoLocationService) { }

  // @Output()
  // private emitAirplaneList: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit() {
    // this.load();
    this.getLocation();
    this.airplaneService.getAirplaneList('Id');
    this.airplaneService.airplane.subscribe((data) => {
      // console.log(data);
      this.airplaneList = [...this.airplaneList, data];
      console.log(data);

    });
  }

  // load() {
  //   const data = JSON.parse(localStorage.getItem('airplaneList'));
  //   if (data === null) {
  //     this.loadData();
  //   } else {
  //       this.airplaneList = data;
  //   }
  // }
  // loadData() {
  //  this.subscriptionLocation = this.location$.subscribe(response => {
  //     this.airplaneService.getData(response.latitude, response.longitude);
  //   });
  //   this.subscriptionAirplaneList = this.airplaneService.airplaneModel$.subscribe(response => {
  //     this.airList = response;
  //     if (this.airList.acList ) {
  //       this.airplaneList = this.airList.acList.sort((obj1, obj2) => obj2.Alt - obj1.Alt);
  //       localStorage.setItem('airplaneList', JSON.stringify(this.airList.acList));
  //     }
  //   });
  // }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.location = position.coords;
        this.locationSource.next(this.location);
      },  this.displayError);
    } else {
        alert('Geolocation is not supported by this browser.');
    }
}

displayError(error) {
  const errors = {
    1: 'Permission denied, please allow the location',
    2: 'Position unavailable',
    3: 'Request timeout'
    };
    alert('Error: ' + errors[error.code]);
  }

  public addAirplaneToAirplaneList(airplaneF: any): void {
    // console.log(airplane);
    this.airplaneList = [...this.airplaneList, airplaneF];
  }
}
