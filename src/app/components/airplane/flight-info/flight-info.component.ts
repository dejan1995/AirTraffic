import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AirplaneService } from '../../../services/airplane.service';

@Component({
  selector: 'app-flight-info',
  templateUrl: './flight-info.component.html',
  styleUrls: ['./flight-info.component.css']
})
export class FlightInfoComponent implements OnInit {
  public id: number;
  public airplane;
  public airplaneList;

  constructor(private activatedRoute: ActivatedRoute, private airplaneService: AirplaneService) { }

  ngOnInit() {
    this.load();
    const flightId = this.activatedRoute.snapshot.params['id'];
  }

  load() {
    this.activatedRoute.params.subscribe(params => {this.id = +params.id; console.log(this.id); });
    this.airplaneList = JSON.parse(localStorage.getItem('airplaneList'));
    this.airplane = this.airplaneList.find(item => item.Id === this.id);
}

}
