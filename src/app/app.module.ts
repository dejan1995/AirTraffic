import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { AirplaneListComponent } from './components/airplane/airplane-list/airplane-list.component';
import { AirplaneService } from './services/airplane.service';

import {HttpClientModule} from '@angular/common/http';
import { GeoLocationService } from './services/geo-location.service';
import { FlightInfoComponent } from './components/airplane/flight-info/flight-info.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: AirplaneListComponent },
  { path: 'airplane/:id', component: FlightInfoComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    FlightInfoComponent,
    AirplaneListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false
      }
    )
  ],
  providers: [GeoLocationService, AirplaneService],
  bootstrap: [AppComponent]
})
export class AppModule { }
