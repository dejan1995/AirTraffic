import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AirplaneService {

  public airplane: Subject<any> = new Subject<any>();
  private airSource: BehaviorSubject<any> = new BehaviorSubject<any>([]);
  public airplaneModel$: Observable<any> = this.airSource.asObservable();

  // tslint:disable-next-line:max-line-length
  // readonly path = 'https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json?lat=51.5073509&lng=-0.12775829999998223&fDstL=0&fDstU=100&Id=Id';

  constructor(private http: HttpClient) { }

  public getAirplaneList(Id: string): void {
    if (Id) {
      const params = new HttpParams().set('Id', Id);

      // tslint:disable-next-line:max-line-length
      this.http.get('https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json?lat=51.5073509&lng=-0.12775829999998223&fDstL=0&fDstU=100',
        { params: params })
        .subscribe(data => {
          this.airplane.next(data);
        });
    }
  }
//   getData(lat, lng) {
//     const params = new HttpParams().set('lat', lat).set('lng', lng)
//   .set('fDstL', '0').set('fDstU', '100');
//   return this.http.get(this.path, { params: params}).subscribe(response => {
//     this.airSource.next(response);
//    });

// }
}
